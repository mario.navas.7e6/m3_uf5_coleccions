import java.util.*

data class Employee(
    var name: String,
    var surname: String,
    var adress: String
)

fun employeeByld(){
    val scanner = Scanner(System.`in`)
    val employees : MutableMap<String, Employee> = mutableMapOf()
    val times = scanner.nextLine().toInt()
    repeat(times){
        val dni = scanner.nextLine()
        val employee = Employee(scanner.nextLine(),scanner.nextLine(),scanner.nextLine())
        employees[dni] = employee
    }
    var dni = scanner.nextLine()
    while(dni!="END"){
       println("${employees[dni]?.name} ${employees[dni]?.surname} - $dni, ${employees[dni]?.adress}")
       dni= scanner.nextLine()
    }
}