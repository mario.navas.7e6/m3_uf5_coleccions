import java.util.*

fun delegationVoting(){
    val scanner = Scanner(System.`in`)
    var delegates : MutableMap<String, Int> = mutableMapOf()
    var input = scanner.next()
    while(input!= "END"){
        if(input in delegates){
            delegates[input] = delegates[input]!! + 1
        }else{
            delegates.put(input, 1)
        }
        input = scanner.next()
    }
    for(i in delegates){
        println("${i.key}: ${i.value}")
    }
}