import java.util.*

fun repeatedAnswer(){
    val scanner = Scanner(System.`in`)
    val answers: MutableSet<String> = mutableSetOf()
    var input = scanner.next()
    while(input!="END"){
        if(answers.contains(input)) println("MEEEC!")
        else answers += input
        input = scanner.next()
    }
}